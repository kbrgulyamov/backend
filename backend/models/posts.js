const mongoose = require('mongoose')


const Posts = mongoose.Schema({
      title: {
            type: String
      },
      description: {
            type: String
      },
      comments: {
            type: String
      },
      data: {
            data: new Date().getDay()
      }

})

module.exports = mongoose.model('Posts', Posts)