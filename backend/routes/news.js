const express = require("express")
const router = express.Router()

router.get('/', (req, res) => {
      res.send({
            title: {
                  type: String
            },
            description: {
                  type: String
            },
            comments: {
                  type: String
            },
            data: {
                  data: new Date().getDay()
            }
      })
})


module.exports = router