// Импортируем наш фреймворк
const express = require("express")
// Создаем главную переменную

const app = express()
const cors = require('cors')
const bodyParser = require("body-parser")
const mongoose = require('mongoose')


// Middlewares
app.use(cors())
app.use(bodyParser.json())



// Routes
app.use('/users', require('./routes/users.js'))
app.use('/posts', require('./routes/posts.js'))
app.use('/news', require('./routes/news.js'))



//Loading with BD
mongoose.connect("mongodb://localhost:27017", () => {
      console.log("Loading Bd")
})

mongoose.set("debug", true)

//Слушатель по порту 
app.listen(1212, () => {
      console.log('Port 1212!')
})